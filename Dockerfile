FROM gradle:8-jdk17 as build
WORKDIR /com.kavak.poc:jooby-kotlin:1.0.0
COPY build.gradle build.gradle
COPY settings.gradle settings.gradle
COPY src src
COPY conf conf
RUN gradle shadowJar

FROM eclipse-temurin:17-jdk
WORKDIR /com.kavak.poc:jooby-kotlin:1.0.0
COPY --from=build /com.kavak.poc:jooby-kotlin:1.0.0/build/libs/com.kavak.poc:jooby-kotlin:1.0.0-1.0.0-all.jar app.jar
COPY conf conf
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]
