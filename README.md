# com.kavak.poc:jooby-kotlin:1.0.0

Welcome to com.kavak.poc:jooby-kotlin:1.0.0!!

## running

    ./gradlew joobyRun

## building

    ./gradlew build

## docker

     docker build . -t com.kavak.poc:jooby-kotlin:1.0.0
     docker run -p 8080:8080 -it com.kavak.poc:jooby-kotlin:1.0.0
