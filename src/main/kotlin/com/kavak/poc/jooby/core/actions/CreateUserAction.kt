package com.kavak.poc.jooby.core.actions

import com.kavak.poc.jooby.core.model.domain.UserValueObject
import com.kavak.poc.jooby.core.model.repositories.UserRepository

class CreateUserAction(
        private val userRepository: UserRepository
) {

    suspend fun createUser(user: UserValueObject) = userRepository.create(user)

}