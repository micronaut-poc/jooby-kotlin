package com.kavak.poc.jooby.core.model.repositories

import com.kavak.poc.jooby.core.model.domain.User
import com.kavak.poc.jooby.core.model.domain.UserValueObject
import java.util.UUID

interface UserRepository {
    suspend fun find(id: UUID): User?
    suspend fun create(user: UserValueObject): User
}