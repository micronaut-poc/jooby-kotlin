package com.kavak.poc.jooby.core.model.domain

import java.util.UUID

data class User(
        val id: UUID,
        val name: String,
        val lastName: String,
        val status: String
)

data class UserValueObject(
        val name: String,
        val lastName: String,
        val status: String
)
