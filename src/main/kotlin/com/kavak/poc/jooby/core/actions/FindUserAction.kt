package com.kavak.poc.jooby.core.actions

import com.kavak.poc.jooby.core.model.repositories.UserRepository
import java.util.UUID

class FindUserAction(
        private val userRepository: UserRepository
) {

    suspend fun find(id: UUID) = userRepository.find(id) ?: throw NoSuchElementException()

}