package com.kavak.poc.jooby.core.infrastructure

import com.kavak.poc.jooby.core.model.domain.User
import com.kavak.poc.jooby.core.model.domain.UserValueObject
import com.kavak.poc.jooby.core.model.repositories.UserRepository
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

class DbUserRepository(
    private val db: Database
) : UserRepository {
    override suspend fun find(id: UUID): User? {
        return transaction(db) {
            UserTable.select { UserTable.id eq id }
                .singleOrNull()
                ?.let {
                    User(
                        id = it[UserTable.id],
                        name = it[UserTable.name],
                        lastName = it[UserTable.lastName],
                        status = it[UserTable.status])
                }
        }
    }

    override suspend fun create(user: UserValueObject): User {
        return transaction(db) {
            UserTable.insert {
                it[name] = user.name
                it[lastName] = user.lastName
                it[status] = user.status
            }.let {
                User(it[UserTable.id], user.name, user.lastName, user.status)
            }
        }
    }
}

object UserTable : Table("users") {
    val id = uuid("id").autoGenerate()
    val name = varchar("name", 255)
    val lastName = varchar("last_name", 255)
    val status = varchar("status", 255)
    override val primaryKey = PrimaryKey(id, name = "pk_user_id")
}