package com.kavak.poc.jooby.core.infrastructure

import com.kavak.poc.jooby.core.model.domain.User
import com.kavak.poc.jooby.core.model.domain.UserValueObject
import com.kavak.poc.jooby.core.model.repositories.UserRepository
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import java.util.UUID

class ApiUserRepository(
        private val httpClient: HttpClient
) : UserRepository {

    override suspend fun find(id: UUID): User {
        return httpClient.get("/users/${id}").body()
    }

    override suspend fun create(user: UserValueObject): User {
        return httpClient.post("/users") {
            contentType(ContentType.Application.Json)
            setBody(user)
        }.body()
    }
}