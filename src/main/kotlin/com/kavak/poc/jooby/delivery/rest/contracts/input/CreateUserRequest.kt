package com.kavak.poc.jooby.delivery.rest.contracts.input

import com.kavak.poc.jooby.core.model.domain.UserValueObject

data class CreateUserRequest(
        val name: String,
        val lastName: String,
        val status: String
) {
    fun toUserValueObject() = UserValueObject(name, lastName, status)
}
