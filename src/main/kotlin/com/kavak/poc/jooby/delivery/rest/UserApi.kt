package com.kavak.poc.jooby.delivery.rest

import com.kavak.poc.jooby.core.actions.CreateUserAction
import com.kavak.poc.jooby.core.actions.FindUserAction
import com.kavak.poc.jooby.delivery.rest.contracts.input.CreateUserRequest
import io.jooby.kt.Kooby
import io.jooby.kt.body
import java.util.*
import java.util.concurrent.Executors

class UserApi(
        private val findAction: FindUserAction,
        private val createAction: CreateUserAction
) : Kooby({
    worker = Executors.newCachedThreadPool()

    coroutine {
        path("/users") {
            get("/{id}") {
                findAction.find(UUID.fromString(ctx.path("id").value()))
            }

            post("/") {
                val createRequest = ctx.body(CreateUserRequest::class)
                createAction.createUser(createRequest.toUserValueObject())
            }
        }
    }
})