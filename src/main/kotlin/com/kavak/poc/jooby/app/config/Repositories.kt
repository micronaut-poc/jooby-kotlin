package com.kavak.poc.jooby.app.config

import com.kavak.poc.jooby.core.infrastructure.ApiUserRepository
import com.kavak.poc.jooby.core.infrastructure.DbUserRepository

object Repositories {
    val userApiRepository by lazy {
        ApiUserRepository(HttpClients.mockUserApiClient)
    }

    val userDbRepository by lazy {
        DbUserRepository(Databases.database)
    }
}