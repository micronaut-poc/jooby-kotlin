package com.kavak.poc.jooby.app.config

import com.kavak.poc.jooby.core.infrastructure.UserTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction

object Databases {
    val database by lazy {
        val db = Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver")

        transaction(db) {
            addLogger(StdOutSqlLogger)
            SchemaUtils.create(UserTable)
        }

        db
    }
}