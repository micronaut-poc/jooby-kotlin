package com.kavak.poc.jooby.app.config

import com.kavak.poc.jooby.core.actions.CreateUserAction
import com.kavak.poc.jooby.core.actions.FindUserAction
import com.kavak.poc.jooby.delivery.rest.UserApi

object RestApis {
    val userApi by lazy {
        UserApi(
            FindUserAction(Repositories.userApiRepository),
            CreateUserAction(Repositories.userApiRepository)
        )
    }

    val userApiV2 by lazy {
        UserApi(
            FindUserAction(Repositories.userDbRepository),
            CreateUserAction(Repositories.userDbRepository)
        )
    }
}