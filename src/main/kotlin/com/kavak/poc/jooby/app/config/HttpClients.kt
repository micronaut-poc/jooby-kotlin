package com.kavak.poc.jooby.app.config

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache5.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.jackson.*

object HttpClients {
    val mockUserApiClient by lazy {
        HttpClient(Apache5) {
            install(Logging)
            install(ContentNegotiation) {
                jackson {
                    propertyNamingStrategy = PropertyNamingStrategies.SNAKE_CASE
                }
            }

            engine {
                connectTimeout = 10_000
                connectionRequestTimeout = 10_000
                socketTimeout = 10_000
            }

            defaultRequest {
                url("http://localhost:9001")
                accept(ContentType.Application.Json)
            }
        }
    }
}