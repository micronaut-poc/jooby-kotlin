package com.kavak.poc.jooby

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.module.kotlin.KotlinFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.kavak.poc.jooby.app.config.RestApis
import io.jooby.ExecutionMode
import io.jooby.jackson.JacksonModule
import io.jooby.kt.Kooby
import io.jooby.kt.runApp
import io.jooby.undertow.UndertowServer

class App : Kooby(
        {
            executionMode = ExecutionMode.EVENT_LOOP

            install(UndertowServer())
            install(JacksonModule(buildObjectMapper()))

            mount("/v1", RestApis.userApi)
            mount("/v2", RestApis.userApiV2)

            coroutine {
                get("/health") {
                    "I'm ok!"
                }
            }
        }
)

fun buildObjectMapper(): ObjectMapper {
    return ObjectMapper()
            .setPropertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE)
            .registerModules(KotlinModule.Builder()
                    .withReflectionCacheSize(512)
                    .configure(KotlinFeature.NullToEmptyCollection, false)
                    .configure(KotlinFeature.NullToEmptyMap, false)
                    .configure(KotlinFeature.NullIsSameAsDefault, false)
                    .configure(KotlinFeature.StrictNullChecks, false)
                    .build()
            )
}

fun main(args: Array<String>) {
    runApp(args, ::App)
}
